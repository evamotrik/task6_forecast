class WeatherWidget {
    constructor(api){
        this.api = api;
    }
    async getWeather(coords){
        let param = 'lat=' + parseFloat((coords.latitude).toFixed(6)) + '&lon=' + parseFloat((coords.longitude).toFixed(6));
        this.api = 'http://api.openweathermap.org/data/2.5/weather?lang=ru&' + param + '&appid=50bcaf683f838ee228abb1b56d58aa6d';
       await fetch(this.api).then(resp => {
            return resp.json()
        
        }).then(resp => {
            console.log(resp)
        document.querySelector('.temp0').innerHTML = Math.round(resp.main.temp - 273) + '&deg;';
        document.querySelector('.city').innerHTML = resp.name;
        document.querySelector('.img').innerHTML = `<img src="https://openweathermap.org/img/wn/${resp.weather[0]['icon']}@2x.png">`;
        document.querySelector('.humidity').innerHTML = `Влажность: ${resp.main.humidity}`;
        document.querySelector('.wind').innerHTML = `Ветер: ${resp.wind.speed}`;
        document.querySelector('.info').textContent = resp.weather[0]['description'];
    })
    .catch(function() {   
    });
    }

     getWeatherForecast(coords){
        let param = 'lat=' + parseFloat((coords.latitude).toFixed(6)) + '&lon=' + parseFloat((coords.longitude).toFixed(6));
        this.api = 'http://api.openweathermap.org/data/2.5/forecast?lang=ru&' + param + '&appid=50bcaf683f838ee228abb1b56d58aa6d';
     fetch(this.api).then(resp => {
            return resp.json()
        }).then(resp => {
            console.log(resp)
            let arr = [];
            let s = '';
            let counter = 0;
            for(let i = 0; i < resp.list.length; i++){
                if(resp.list[i].dt_txt.includes('12:00:00')){  
                    if(counter == 3){
                        break;
                    }
                    else{
                        arr.push(resp.list[i]);
                    }
                    counter++;
                }
            }

            for(let j = 0; j < arr.length; j++){
            let temp0 = Math.round(arr[0].main.temp - 273) + '&deg;';
            let date = arr[j].dt_txt;
            let img = `<img src="https://openweathermap.org/img/wn/${arr[j].weather[0]['icon']}@2x.png">`;
            let wind = arr[j].wind.speed;
            let humidity = arr[j].main.humidity;
            s += `
            <div class="weather_today">
            <div class="main_info">
              <div class="date_forecast">${date}</div>
              <div class = "img_forecast">${img}</div>
              <h3 class="temp0_forecast">${temp0}</h3>
              <span class="humidity_forecast">${humidity}</span>%
              <span class="wind_today">${wind}</span>м/с 
            </div> 
          </div>`
            }

            document.querySelector('.days').innerHTML = s;
            
    })
    .catch(function() {   
    });
    }
}

const catchWeather = new WeatherWidget();

    window.onload = function() { 
    
        document.body.classList.add('loaded_hiding');
        window.setTimeout(function () {
          document.body.classList.add('loaded');
          document.body.classList.remove('loaded_hiding');
        }, 500);

        navigator.geolocation.getCurrentPosition(({coords}) => {  
            console.log(coords);
            catchWeather.getWeather(coords);
            catchWeather.getWeatherForecast(coords);
        })
      }  
      function viewForecast(){
        document.getElementById("days").style.display = "block";
    }   
    






